--Creates a view of all active security cameras

CREATE VIEW ActiveSecurityCameras AS
SELECT camera_model.model_id, camera_model.recording, device.year_manufac
FROM camera_model JOIN device ON camera_model.model_id = device.model_id
WHERE device.active = 1;

--Example query to find active security cameras that were manufactured before 2015
Select year_manufac
From ActiveSecurityCameras
Where year_manufac < 2015;

--Creates a view keeping track of new devices' serial number, model Id, and year it was manufactured
CREATE VIEW CountNewDevices AS
Select serial_number, model_id, year_manufac
From device 
Where year_manufac > 2018;

--Example query on the CountNewDevices view
Select * From CountNewDevices;
