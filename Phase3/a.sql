/*Finds which department averages the most custom ram*/
SELECT T1.name, MAX(avg_ram)
FROM
    (SELECT d.name, AVG(de.custom_ram) AS avg_ram
    FROM department AS d
        INNER JOIN owner AS o
            ON d.dept_id = o.dept_id
        INNER JOIN device AS de
            ON o.serial_number= de.serial_number
    GROUP BY d.name) AS T1;


/*Finds the building with the most printers*/
SELECT T.name, MAX(number_of_printers)
FROM(
    SELECT building.name as name, COUNT(*) AS number_of_printers
    FROM device
        INNER JOIN printer_model
            ON printer_model.model_id = device.model_id
        INNER JOIN owner
            ON device.serial_number = owner.serial_number
        INNER JOIN room
            ON owner.room_id = room.room_id
        INNER JOIN building
            ON room.bldg_id = building.bldg_id
    GROUP BY building.name) AS T;


/*Ranks people by the number of devices they have*/
SELECT p.first_name, p.last_name, COUNT(*) AS device_count
FROM device AS d
    INNER JOIN owner AS o
        ON d.serial_number = o.serial_number
    INNER JOIN person AS p
        ON p.person_id = o.person_id
GROUP BY p.person_id
ORDER BY device_count DESC;


/*Finds the computer model with the greatest number of inactive devices*/
SELECT T2.model_id, MAX(number_of_devices)
FROM
    (SELECT cm.model_id, COUNT(*) AS number_of_devices
    FROM device AS d
        INNER JOIN computer_model AS cm
            ON d.model_id = cm.model_id
    WHERE d.active = FALSE
    GROUP BY cm.model_id) AS T2;


/*Finds all the computer devices of a specific manufacturer, in this case the manufacturer 'Apple'*/
SELECT computer_model.model_id, device.custom_cpu, device.custom_ram, computer_model.cpu, computer_model.ram
FROM device
    INNER JOIN computer_model 
        ON device.model_id = computer_model.model_id 
    INNER JOIN manufacturer 
        ON computer_model.manuf_id = manufacturer.manuf_id
WHERE manufacturer.name = 'Apple';
