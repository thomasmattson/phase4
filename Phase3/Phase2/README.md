## __Work of Adeline Ndacyayisenga, Didi Bulow, Levi Nelson, and Thomas Mattson__

---

### Description

> Levi Nelson created a script that takes the sample data from the csv folder and converts it into an SQL query. The group supplied the data for each csv file. We did not enclude the erd file because of no changes to the original diagram.

> To run script:

```
python3 populate.py
```