import csv
import os
import sys

def trim_string(string):
  str_en = string.encode('ascii', 'ignore')
  str_de = str_en.decode()
  
  return str_de.strip()


def filter(x):
  if ("TRUE" in x) or ("FALSE") in x:
    return x
  else:
    if x.isnumeric():
      if len(x) < 10:
        return x
      else:
        return '"'+ x + '"'
    else:
      return '"'+ x + '"'

def make_sql(csvFileName, sql, table):
  openFile = open(csvFileName, 'r')
  openFileSql = open(sql, 'a')
  csvFile = csv.reader(openFile)
  header = next(csvFile)
  headers = map((lambda x: trim_string(x)), header)
  insert = 'INSERT INTO ' + table + ' (' + ", ".join(headers) + ") VALUES "
  for row in csvFile:
      values = map((lambda x: filter(trim_string(x))), row)
      openFileSql.write(insert +"("+ ", ".join(values) +");\n")
  openFile.close()
  openFileSql.close()

if __name__ == '__main__':
  files = os.listdir('CSV_Data')
  for f in files:
    if ('.csv' in f):
      table = f.replace('.csv', '')
      if (not 'person' in f) and (not 'device.' in f):
        make_sql('./CSV_Data/' + f, 'populatesmall.sql', table)
      if ('person' in f) or ('device.' in f):
        make_sql('./CSV_Data/' + f, 'populatelarge.sql', table)
    