/*Timing is automatically output in mySQL*/
/*INDEX 1*/
CREATE INDEX idx_person_id ON person (first_name, last_name);

/*QUERY*/
SELECT p.first_name, p.last_name, COUNT(*) AS device_count
FROM device AS d
INNER JOIN owner AS o
ON d.serial_number = o.serial_number
INNER JOIN person AS p
ON p.person_id = o.person_id
GROUP BY p.person_id
ORDER BY device_count DESC;

/*Drop index statement*/
DROP INDEX idx_person_id

/*QUERY 1*/
SELECT p.first_name, p.last_name, COUNT(*) AS device_count
FROM device AS d
INNER JOIN owner AS o
ON d.serial_number = o.serial_number
INNER JOIN person AS p
ON p.person_id = o.person_id
GROUP BY p.person_id
ORDER BY device_count DESC;

/*Time to run without index: 1.127 seconds*/

/*Time to run with index: 0.043 seconds*/

/*By indexing the attributes first_name and last_name of the relation person, the query
which ranked people by the number of devices they had ran significantly faster*/


/*INDEX 2*/
CREATE INDEX idx_bldg_name_id ON building (name, bldg_id);

/*QUERY 2*/
SELECT T.name, MAX(number_of_printers)
FROM(
SELECT building.name as name, COUNT(*) AS number_of_printers
FROM device
INNER JOIN printer_model
ON printer_model.model_id = device.model_id
INNER JOIN owner
ON device.serial_number = owner.serial_number
INNER JOIN room
ON owner.room_id = room.room_id
INNER JOIN building
ON room.bldg_id = building.bldg_id
GROUP BY building.name) AS T;

/*Drop index statement*/
DROP INDEX idx_bldg_name_id

/*QUERY*/
SELECT T.name, MAX(number_of_printers)
FROM(
SELECT building.name as name, COUNT(*) AS number_of_printers
FROM device
INNER JOIN printer_model
ON printer_model.model_id = device.model_id
INNER JOIN owner
ON device.serial_number = owner.serial_number
INNER JOIN room
ON owner.room_id = room.room_id
INNER JOIN building
ON room.bldg_id = building.bldg_id
GROUP BY building.name) AS T;

/*Time to run without index: 3 minutes 16.549 seconds*/

/*Time to run with index: 2 minutes 27.248 seconds*/
