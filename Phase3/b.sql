--Delete all inactive devices

DELETE FROM Device
Where active = False;

--Update all devices with an un-upgraded model to be inactive. In case all devices' 
--models change, we might need to keep track of the inactive devices(with unupaded models)
UPDATE device
Set active = false
Where model_id < 1000;

-- Update custom cpu of all computers with a certain screen size. Can be used to update specific devices 
--based on the updates needed 
UPDATE device
Set custom_cpu = 50
Where device.model_id IN (Select model_id
                        From computer_model
                        Where screen_size < 700);

--Inserts a specific person to be the department head of a specific department.
--In this example the person desired has first name Terry last name Rose and is
--to be made the head of the department Kare

UPDATE department
SET department.head = (SELECT concat(p.first_name," ",p.last_name)
                        FROM person as p
                        WHERE p.first_name = "Terry" AND p.last_name = "ROSE")
Where department.name = "Kare";

--Updates all the operating systems of devices of a specific model for those models that have an os
--below a certain threshold. In this case it is updating all the computer devices that have an os below 70.
--This is based off of the notion that the operating system of a number of similar devices might be updated at the same time and this change would want to be reflected
 
UPDATE device
SET device.os = 100
WHERE device.os < 70 and device.model_id IN (SELECT computer_model.model_id
FROM computer_model);