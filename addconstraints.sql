ALTER TABLE projector_model 
ADD CONSTRAINT CHK_hdmi CHECK (hdmi<=3);
​
ALTER TABLE monitor_model
ADD CONSTRAINT CHK_panel CHECK (panel_type in ('VA','IPS','TN','PLS','AHVA'));
​
ALTER TABLE camera_model
ADD CONSTRAINT CHK_record CHECK (recording in('0','1') AND field_of_view<=180);
